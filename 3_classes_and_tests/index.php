<?php
/*
* опишите что этот код делает и найдите ошибки если они есть
 *
 * Рассылка писем нескольким получателям через почтовый сервис postmarkapp
 *
 *
 * Массив для рассылки emails получаются с базы данных
 * Получаем поля с таблицы у которых статус 0 или 3, сгрупировать по полю status в порядке повышения, отсортировать по полю crtime в порядке повышения, ограничить количество 10 записями
 *
 * Если данные были получены формируем массив для отправки
 *
 * Перед отправкой меняем статус на EMAIL_STATUS_SENDING
 *
 * Отправляем массив с данными для рассылки на сервис postmarkapp методом POST и указываем что ожидаем ответ в json формате
 *
 * Полученый ответ декодируем с json строки в массив
 * Перебераем массив с результатами отправки
 * Проверяем на наличие информации про ошибки в отправке
 * и если есть со статусом "Found inactive addresses"
 * то вносим данный email в таблицу EMAILS_SPAMMED_TABLE если его там еще нет
 *
 * формируем массив с id удачных уведомлений и уведомлений с ошибками
 *
 * Меняем статус заказов на EMAIL_STATUS_SENT для записей по которым была удачная рассылка
 *
 * Ошиибки
 * в функции change_statuses условие  if (!$ids) return true; никогда не будет выполнятся, $ids если будет пустым массивом все равно условие будет false
 *
 * Замечания
 *
 * $desc  = $mail['description']; переменная $desc не используется в коде, $mail['description'] можно перенести в $prepared_mails
 *
 * После отправки curl запроса нет проверки на ошибку запроса !curl_errno($ch)
 * Перед foreach($server_output AS $index => $mail) нудна проверка на наличие данных для перебора
*/
use vegcoders\core\db\DB;
# SETTINGS #
define('POSTMARK_TOKEN',	'some code');

# PREPARES #
$select_sql     = 'SELECT * FROM ' . EMAILS_TABLE . ' WHERE status IN (0,3) ORDER BY status ASC, crtime ASC LIMIT 10';
$prepared_mails = array(); // Sending format for API
$ids      		= array(); // For updating statuses


$to_send = DB::query($select_sql, 'array');
if ($to_send) {
    $ids = array();
    foreach ($to_send as $index => $mail) {
        $ids[$index] = $mail['id'];
        $desc  = $mail['description']; // нет смысла в этой переменной
        $prepared_mails[] = array(
            'From'     => ADMIN_EMAIL,
            'To'       => $mail['to_email'],
            'Subject'  => $mail['subject'],
            'HtmlBody' => $desc,
        );
    }
    echo date('Y-m-d H:i:s') . ' STARTED EMAILS ' . implode(',', $ids) . PHP_EOL;

    # MAILING #
    change_statuses($ids, EMAIL_STATUS_SENDING);
    $return = send_batch($ids, $prepared_mails);
    if ($return['ok']) {
        change_statuses($return['ok'],   EMAIL_STATUS_SENT);
    }
    echo date('Y-m-d H:i:s') . ' PENDING MAIL SENT ' . count($return['ok']) . ' / ERRORS ' . count($return['bad']) . PHP_EOL;
}

function change_statuses(array $ids, $new_status)
{
    if (!$ids) return true; //никогда не будет выполнятся, $ids если будет пустым массивом все равно условие будет false
    return DB::query('UPDATE ' . EMAILS_TABLE . ' SET status = ' . $new_status . ', chtime = NOW() WHERE id IN (' . implode(',', $ids) . ')');
}

function send_batch(array $ids, $emails)
{
    $ch = curl_init('https://api.postmarkapp.com/email/batch');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($emails));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Accept: application/json',
        'Content-Type: application/json',
        'X-Postmark-Server-Token: ' . POSTMARK_TOKEN
    ));
    $server_output = json_decode(curl_exec($ch), true); //нехватает !curl_errno($ch)
    curl_close($ch);
    $return = array('ok' => array(), 'bad' => array());
    foreach($server_output AS $index => $mail) { //нет обработки условия если будет ошибка в ответе, может вызвать исключение
        $db_id = $ids[$index];
        if (!isset($mail['ErrorCode']) || !$mail['ErrorCode']) {
            $return['ok'][$db_id] = $db_id;
        } else {
            $message = $mail['Message'];
            $status = EMAIL_STATUS_FAILED;
            if (!(strpos($message, 'Error parsing')) === false) {
                $status = EMAIL_STATUS_WRONG_TO;
            } elseif (!(strpos($message, 'Found inactive addresses')) === false) {
                $status = EMAIL_STATUS_SPAMMED;
                $to = $emails[$index]['To'];
                if(!DB::query('SELECT id FROM '. EMAILS_SPAMMED_TABLE . ' WHERE email='.DB::escape($to, true))) {
                    DB::insert(EMAILS_SPAMMED_TABLE, array('email' => $to));
                    echo 'ADDED TO SPAMMED ' . $to . PHP_EOL;
                }
            }
            $return['bad'][$db_id] = array(
                'status'  => $status,
                'message' => $message
            );
        }
    }
    return $return;
}