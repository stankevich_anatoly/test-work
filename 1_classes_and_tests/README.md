# Installing

### Clone the project and run composer install

cd test-work/1_classes_and_tests
composer install

---

### Run with built-in web server to specify document root <docroot> for built-in web server.

> Настройки в гугле для ответа настроена для http//localhost:8000
>> Нужно назначить дефолтный (localhost:8000) url для папки проекта 1_classes_and_tests\public
>> php -S localhost:8000 -t D:\www\test_work\test-work\1_classes_and_tests\public

php -S localhost:8000 -t <docroot>

---

### Google APIs Client Library for PHP
https://github.com/googleapis/google-api-php-client
