<?php

class Drive
{
    const CLIENT_SECRET = 'credentials.json';
    const REDIRECT_URL = 'http://localhost:8000';
    const APPLICATION_NAME = 'my-project-67748';

    /**
     * Get the API client.
     * @var Google_Client
     */
    static private $client;

    /**
     * @var null
     */
    protected static $instance = null;

    /**
     * gate modifiedTime for sort
     * @var null
     */
    static private $modifiedTime = null;

    /**
     * Drive constructor.
     * Get the API client
     * @param mixed $code
     *
     * @throws Exception
     */
    public function __construct($code = false){
        if (self::$instance === null){
            try {
                $client = new Google_Client();
                $client->setRedirectUri(self::REDIRECT_URL);
                $client->setApplicationName(self::APPLICATION_NAME);
                $client->addScope([
                    Google_Service_Sheets::SPREADSHEETS,
                    Google_Service_Sheets::DRIVE,
                    Google_Service_Sheets::DRIVE_FILE
                ]);
                $client->setAuthConfig(CLIENT_SECRET_PATH . self::CLIENT_SECRET);
                $client->setAccessType('offline');
                $client->setPrompt('select_account consent');

                // Load previously authorized token from a file, if it exists.
                // The file token.json stores the user's access and refresh tokens, and is
                // created automatically when the authorization flow completes for the first
                // time.
                $tokenPath = 'token.json';
                if (file_exists($tokenPath)) {
                    $accessToken = json_decode(file_get_contents($tokenPath), true);
                    $client->setAccessToken($accessToken);
                }

                // If there is no previous token or it's expired.
                if ($client->isAccessTokenExpired()) {
                    // Refresh the token if possible, else fetch a new one.
                    if ($client->getRefreshToken()) {
                        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
                    } else {
                        // Request authorization from the user.
                        if (!$code) {
                            $authUrl = $client->createAuthUrl();
                            header('Location: '.$authUrl);
                        }
                        // Exchange authorization code for an access token.
                        $accessToken = $client->fetchAccessTokenWithAuthCode($code);
                        $client->setAccessToken($accessToken);

                        // Check to see if there was an error.
                        if (array_key_exists('error', $accessToken)) {
                            throw new Exception(join(', ', $accessToken));
                        }
                    }
                    // Save the token to a file.
                    if (!file_exists(dirname($tokenPath))) {
                        mkdir(dirname($tokenPath), 0700, true);
                    }
                    file_put_contents($tokenPath, json_encode($client->getAccessToken()));
                }

                self::$client = $client;
                self::$modifiedTime = date("Y-m-d\TH:i:s", mktime(0, 0, 0, date('m')-1, date('d'), date('Y')));;

            } catch (ErrorException $e) {
                throw new Exception ($e->getMessage());
            }
        }
        return self::$instance;
    }

    /**
     * Lists or searches files. (files.listFiles)
     * @return Google_Service_Drive_FileList
     */
    public function getData(){
        // Construct the service object.
        $service = new Google_Service_Drive(self::$client);

        // Params for Print the names, IDs and modifiedTime for created or modified per month.
        $optParams = array(
            'orderBy'   => 'modifiedTime desc',
            'fields'    => 'nextPageToken, files(id, name, modifiedTime)'
        );

        if(self::$modifiedTime){
            $optParams['q'] =  "modifiedTime > '".self::$modifiedTime."'";
        }

        // Get lists the user's files
        return $service->files->listFiles($optParams);
    }

}