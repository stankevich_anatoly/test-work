<?php
/**
 * Website document root
 */
define('APPROOT', __DIR__.DIRECTORY_SEPARATOR);
define("CLIENT_SECRET_PATH", APPROOT . "../config".DIRECTORY_SEPARATOR);

require APPROOT . '../vendor/autoload.php';
require APPROOT . 'Drive.php';

$code = false;
if(isset($_GET["code"])){
    $code = $_GET["code"];
}

$drive = new Drive($code);
$results = $drive->getData();

if (count($results->getFiles()) == 0) {
    print "No files found.<br>";
} else {
    print "Files:<br>";
    foreach ($results->getFiles() as $file) {
        $date = new DateTime($file['modifiedTime']);
        printf("%s - %s (%s)<br>", $file->getName(), date_format($date, 'Y-m-d H:i:s'), $file->getId());
    }
}
