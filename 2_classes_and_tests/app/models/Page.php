<?php

namespace App\Models;

use \App\Core\Model;

class Page extends Model
{
    public function getById($id)
    {
        return $this->findOne("SELECT * FROM pages WHERE id = '".$id."'");
    }

    public function getByFriendly($friendly)
    {
        return $this->findOne("SELECT * FROM pages WHERE friendly = '". $friendly."'");
    }

    public function getAll()
    {
        return $this->findMany("SELECT id, title, friendly FROM pages");
    }
}
