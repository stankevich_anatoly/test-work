<?php

namespace App\Core;

class Track
{
    private $controller;
    private $action;
    private $params;

    public function __construct($controller, $action, $params = [])
    {
        $this->controller = $controller;
        $this->action = $action;
        $this->params = $params;
    }

    /**
     * Magic get method to allow getting class properties but still having them protected
     * to disallow writing.
     *
     * @return  mixed
     */
    public function __get($property)
    {
        return $this->$property;
    }
}
