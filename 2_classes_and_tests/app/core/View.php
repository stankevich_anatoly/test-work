<?php
namespace app\Core;

class View
{
    public function render(Page $page) {
        return $this->renderLayout($page, $this->renderView($page));
    }

    private function renderLayout(Page $page, $content)
    {
        $layoutPath = APP . "layouts/{$page->layout}.php";

        if (file_exists($layoutPath)) {
            ob_start();
                $title = $page->title;
                $content = $content;
                include $layoutPath;
            return ob_get_clean();
        } else {
            echo "File with layout on path not found $layoutPath"; die();
        }
    }

    private function renderView(Page $page)
    {
        if ($page->view) {
            $viewPath = APP . "views/{$page->view}.php";

            if (file_exists($viewPath)) {
                ob_start();
                    $data = $page->data;
                    extract($data);
                    include $viewPath;
                return ob_get_clean();
            } else {
                echo "File with view on path not found $viewPath"; die();
            }
        }
    }
}
