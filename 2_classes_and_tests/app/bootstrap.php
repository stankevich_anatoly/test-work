<?php

namespace App\Core;

require_once APP . 'config/connection.php';

spl_autoload_register(function($class) {
    preg_match('#(.+)\\\\(.+?)$#', $class, $match);

    $nameSpace = str_replace('\\', DIRECTORY_SEPARATOR, strtolower($match[1]));
    $className = $match[2];

    $path = ROOT . $nameSpace . DIRECTORY_SEPARATOR . $className . '.php';

    if (file_exists($path)) {
        require_once $path;

        if (class_exists($class, false)) {
            return true;
        } else {
            throw new \Exception("Class $class not found $path. Check the spelling of the class name inside the specified file.");
        }
    } else {
        throw new \Exception("For $class not found $path. Check for the existence of the file at the specified path. Make sure that the namespace of your class is the same as the one the framework is trying to find for the given class. For example, you create a model class, but forgot to use it. In this case, you are trying to call a model class in the controller namespace and there is no such file.");
    }
});

$routes = require APP . 'config/routes.php';

$track = ( new Router )->getTrack($routes, $_SERVER['REQUEST_URI']);
$page  = ( new Dispatcher )->getPage($track);

echo (new View)->render($page);