<?php

namespace App\Controllers;

use App\Core\Controller;
use App\Models\Page;

class PageController extends Controller
{
    public function index($args)
    {
        $page = (new Page)->getByFriendly($args['friendly']);

        if(!empty($page)){
            $this->title = $page['title'];
            return $this->render('page/index', $page);
        }
        return (new ErrorController)->notFound();
    }
}
