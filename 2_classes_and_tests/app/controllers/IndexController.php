<?php

namespace App\Controllers;

use App\Core\Controller;
use App\Models\Page;


class IndexController extends Controller
{
    private $pages;

    public function __construct()
    {
        $this->title = 'Wellcome';
        $this->pages = (new Page)->getAll();
        if(empty($this->pages)){
           return (new ErrorController)->notFound();
        }
    }

    public function index()
    {
        return $this->render('index/index', $this->pages);
    }
}
