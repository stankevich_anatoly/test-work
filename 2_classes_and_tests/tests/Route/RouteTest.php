<?php

use App\Core\Route;
use PHPUnit\Framework\TestCase;

class RouteTest extends TestCase
{
    private $route;

    protected function setUp() : void
    {
        $this->route = new Route("/","index", "index");
        parent::setUp();
    }

    protected function tearDown() : void
    {
        parent::tearDown();
    }

    /**
     * get $path
     */
    public function testPath(){
        $this->assertEquals("/", $this->route->__get("path"));
        $this->assertNotEquals("/2", $this->route->__get("path"));
    }

    /**
     * get $controller
     */
    public function testController(){
        $this->assertEquals("index", $this->route->__get("controller"));
    }

    /**
     * get $action
     */
    public function testAction(){
        $this->assertEquals("index", $this->route->__get("action"));
    }
}